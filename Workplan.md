# Workplan WorkPi <-> eSSIF

## Highover Work Packages

### Mockups of Screens

Create mockups of screens that will be the basis of discussion within the SSI assurance community on how data can best be exchanged. They are a living document and expected to change often. When everyone is in agreement they will be used as blueprint to develop the software.

### The SSI Interaction Portal

A portal where users can interact with their SSI credentials.
They get an overview of their credentials.
They can receive new credentials issued to them.
They can share credentials with parties interested in them.

### Integration With Clients

A portal where our clients can issue and/or verify credentials.
Information from the client systems should be presented in a secure fashion.
As most parties in our SSI Assurance Community have both the issuer role as well as the verifier role, both issuing and verifying of credentials should be possible.

### Integration With EBSI

Using Walt.ID software we integrate with the EBSI blockchain.
Registering of DID's for all involved parties needs to be done.
Issuing of credentials has to be registered.
When verifying credentials existance on the EBSI blockchain needs to be checked.

### Wallet Development

(Subject to change)
Using custodian software libraries the Portal will contain a native wallet which can receive, hold and share the work related data of individuals.

Another (preferable) option is to find a wallet compatible with the EBSI intrastructure and use this instead.

### Verifiable Credentials Data Modeling

The datamodels of assessments and education (examples given below) have to be modeled into verifiable credentials that are compatible with the EBSI standards.

## Example Usecase

The case WorkPi wants to use as MVP within it's SSI Assurance Community is one where a corporate has en empty vacancy for an executive position, which they set out within an executive search intermediary.
The candidates considered have to be matched based on the requirements. For this existing credentials such as diploma's (BC Diploma?) and skill assessments can be used.
For the requirements unknown the candidates that show high potential can be asked to do an assessment to get a better view of their capabilities. The assessment will be issued to the candidate and the candidate can let the executive search intermediary verify these missing credentials.
When the executive search intermediary forwards the candidate to the corporate they also want to verify the credentials of the candidate.

The detailed flow looks as follows:

### Pre Flow (one time setup)

- Executive Search Intermediary has to register their DID on the EBSI blockchain through Walt.ID powered backend
- Participating Corporates register their DID on the EBSI blockchain through Walt.ID powered backend
- Participating Assessment Parties has to register their DID on the EBSI blockchain through Walt.ID powered backend
- Candidates register their DID on the EBSI blockchain through Walt.ID powered backend

### Main Flow

1. Vacancy is created by corporate (APG) who sources this to the Executive Search Intermediary (Ten, Whyse)
2. Candidates generate a verifiable presentation for Executive Search Intermediary of their existing credentials (if any)
3. Executive Search Intermediary verifies existing credentials of candidates and creates long list of potential prospects
4. Executive Search Intermediary requests assessments for candidates (References, Leadership Performance Assessment) on the long list
5. Assessment party issues the assessment to the candidate who can import this to their wallet.
6. Candidates generate a verifiable presentation of their assessment results for the Executive Search Intermediary
7. Executive Search Intermediary checks results of assessment of all candidates on the long list by verifying the presented credentials
8. Executive Search Intermediary forwards applicable candidate to corporate
9. Candidate shares a verifiable presentation of their credentials with the Corporate
10. Corporate verifies credentials of candidate

## Assurance Community

Our assurance community consists of 2 executive search firms a corporate.

![SSI Assurance Community](./diagrams/SSI_Assurance_community.drawio.svg)

### Current (eSSIF Lab) Scope

- Executive Search
  - TEN
  - Whyz
- Corporate
  - APG

### Future Scope

- Assessment Providers
  - RefQ
  - WorkPi Peer Performance Reviews
  - Twegos
- Education Providers
  - LinkedIn Learning
  - Coursera
  - GoodHabitz
  - Udemy
- Corporate
  - Classified enterprise client in banking sector
  - Classified enterprise client in insurance sector
  - Classified enterprise client in consultancy sector
- Decentralized
  - Work X

## Functional Specification

### 1 Stories applicable to all parties

An Issuer is a party that has or discovers information about a private person (Wallet Holder) which they want to share with that person so that this person can present this information to third parties in a provable manner.

#### 1.1 As any party I want to be able to use a wallet or hardware key as DID so that I can use it for Self Sovereign Identity interactions

- Create an option to connect with a wallet or hardware key (Ubi-key etc.)
  - Connect Button in UI
  - Ability to choose authentiction medium
  - Status indicator in UI showing if connected or error message on failure
  - Display the resulting DID in the users profile in the UI after being connected

#### 1.2 As any Party i want to be able to register my DID on EBSI So that I can Issue, Receive or Verify Credentials

- Authenticate with wallet or key resulting in a DID (using 1.1)
- Use DID with Walt.ID service to register with EBSI
  - Integrate UI with Walt powered backend
- Display result of the registration action to the user

#### 1.3 As any Party i want to be able to check if my DID is registered on EBSI so that I know if it is registered.

- Authenticate with wallet or key resulting in a DID (using 1.1)
- Use DID with Walt.ID service to check with EBSI
  - Integrate UI with Walt powered backend
- Display the DID in the user's profile in the UI with an extra indicator that it has also been registered on EBSI

### 2 Issuer Stories

#### 2.1 As an Issuer i want to be able to issue a credential using my DID so that the credential can be imported by a Wallet Holder

- Ability to specify the information required to be issued in a form/wizard
  - Which information will be issued by which parties?
    - Peer Performance Review
    - Management Performance Assessment
    - Work Experience
    - Display the information in it's final form
- Ability to sign this information using my DID
- Ability to generate attestation that can be imported by a Wallet Holder
  - Option to generate QR code that is diplayed on screen and can be scanned
  - Option to generate link which can be sent to Wallet Holder (optional, if technology allows it, OpenID Provider v2?)
- Use Walt.ID to issue the credential
  - Integrate UI with Walt powered backend

### 3 Wallet Holder Stories

A Wallet Holder is an Individual that can store information about themselves attested by other parties (Issuers) in a Wallet which is under their control, so that they choose what information they want to share with third parties (Verifiers)

#### 3.1 As Wallet Holder I want to be able to receive/import issued credentials about me in my wallet so that i can own and present these credentials

- Add an import interface functionality to the wallet
  - ability to scan a qr code
  - ability to use an import link (OpenID Provider v2?)
  - display imported data after success

#### 3.2 As Wallet Holder I want to be able to present credentials from my wallet to Verifiers so that I can prove these credentials.

- Add a Share button functionality to the wallet
  - Select one or more credentials from wallet
  - Create a verifiable presentation of selected credentials
  - Choose who to share it with
    - DID input field
    - List of "contacts"
    - Ability to scan/search DID registrar (optional, if technology allows it)

### 4 Verifier Stories

A Verifier is a party that is interested in information about a private person (Wallet Holder) and can request them to share this information in a provable way.

#### 4.1 As a Verifier I want to be able to receive shared credentials from Wallet Holders so that I can learn information about them that they willingly shared with me.

- Add ability to receive a verifiable presentation
  - scanning QR code
  - through a link (OpenID Provider v2?)

#### 4.2 As a Verifier I want to be able to verify shared credentials that Wallet Holders shared with me so that I can be sure about the information they shared with me.

- Use Walt.ID to check the validity of the verifiable presentation
- Use Walt.ID to check if the verifiable presentation is know on the EBSI blockchain

## Deliverables

### Mockup Demo

The Mockup Demo will show all user flows, from all roles and perspectives in an interactive fashion. This will be the result of agreement between all stakeholders in the SSI Assurance Community

### Prototype

Working prototype of the application that has all the userinteraction with the software required to make the example use-case as described above.
This software application will have credential issuance, sharing of credentials and verification of those credentials. Also working interactions with the EBSI blockchain through the Walt.ID software.

## Resource Allocation

See Gantt Chart PDF document: WorkPlan_GantChart_WorkPi.pdf in this folder.

## Credential Schemas

During the course of the project our current data models holding the assessment, skills and references data have to be transformed into Verifiable Credentials. These credentials should be EBSI compliant.

Below are the current structures that would have to be transformed.

### Assessment

```ts
import { Measurement } from "./measurement.interface";

export interface Assessment {
  // Name of the assessment
  name: string;

  // key that groups the measurements
  groupingKey: string;

  // Person or Company that provided the assessment
  providerName?: string;

  // Date when the Assessment was completed
  completedDate?: Date;

  // Type of the assessment
  type?: string;

  // Measurements on the assessment
  measurements?: Measurement[];

  // Written reference, for example by a previous employee, describing the capabilities of the individual
  writtenReference?: string;
}
```

### Measurement

```ts
import { Indicator } from "./indicator.interface";

export interface Measurement {
  // Name of the section of the assessment this measurement is about
  name: string;

  // Value of the Measurement if the measurement was numeric
  numberValue?: number;

  // Value of the Measurement if the measurement was text-based
  textValue?: string;

  // Scale of the value of the measurement
  scale?: string;

  // Type of the measurement
  type: string;

  // Indicators affected by this measurement
  indicators?: Indicator[];
}
```

### Indicator

```ts
enum IndicatorCategory {
  SKILL = "SKILL",
  CHARACTERISTIC = "CHARACTERISTIC",
  PREFERENCE = "PREFERENCE",
}

export interface Indicator {
  // Name of the indicator
  name: string;

  // Minlabel of the indicator name
  minLabel?: string;

  // Maxlabel of the indicator name
  maxLabel?: string;

  // Type of the indicator
  type: string;

  // Category of the category
  category: IndicatorCategory;
}
```

### References

- Written Reference

  - `Assessment` is is reused for this.

  - `writtenReference` field is used for this.

  - If the Reference is without skill (indicator) measurements the optional `Assessment.measurements` does not exist.

## Examples

### Skill Assessment

```json
{
  "name": "WorkPi Peer Review",
  "groupingKey": "c4235de3-e35b-4a53-ac46-f62c2b222441",
  "providerName": "WorkPi",
  "completedDate": "2020-11-19T11:36:07.175Z",
  "type": "Assessment",
  "measurements": [
    {
      "name": "Coaching",
      "numberValue": 87,
      "scale": "PERCENTAGE",
      "type": "People Skills",
      "indicators": [
        {
          "name": "Coaching",
          "type": "People Skills",
          "category": "SKILL"
        }
      ]
    },
    {
      "name": "Adaptability",
      "numberValue": 93,
      "scale": "PERCENTAGE",
      "type": "Leadership Capabilities",
      "indicators": [
        {
          "name": "Adaptability",
          "type": "Leadership Capabilities",
          "category": "SKILL"
        }
      ]
    },
    {
      "name": "Working In A Team",
      "numberValue": 93,
      "scale": "PERCENTAGE",
      "type": "People Skills",
      "indicators": [
        {
          "name": "Working In A Team",
          "type": "People Skills",
          "category": "SKILL"
        }
      ]
    }
  ]
}
```

### Written Reference

```json
{
  "name": "Employer Reference",
  "groupingKey": "5e96bdd5-e9db-4ea0-8524-0a41a21526f9",
  "providerName": "Your Previous Company",
  "completedDate": "2020-11-19T11:36:07.175Z",
  "type": "Reference",
  "writtenReference": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}
```
